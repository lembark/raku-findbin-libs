use v6.d;

# CompUnit::RepositoryRegistray is built-in,
# no need to "use" it.

sub EXPORT
(
    *@args  
    --> Map

)
{
    use FileSystem::Parent;

    my $resolve = ? @args.first( 'resolve' );
    my $verbose = ? @args.first( 'verbose' );

    state %scan =
    (
        append      => 'lib',
        filter      => 'dir',
        skip-root   => True
    );

    my @found   
    = scan-up( :$verbose, :$resolve, |%scan );

    for @found.reverse -> $prefix
    {
        CompUnit::RepositoryRegistry.use-repository:
            CompUnit::Repository::FileSystem.new:
                :$prefix
    }

    # caller can look up what's there.

   %( '@FindBin-libs-dirs' => @found )
}

unit module FindBin::libs:ver<0.3.0>:auth<CPAN:lembark>;
=finish
