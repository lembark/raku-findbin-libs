[![Build Status](https://travis-ci.org/lembark/raku-findbin-libs.svg?branch=master)](https://travis-ci.org/lembark/raku-findbin-libs)

NAME
====

FindBin::libs - add ./lib dirs below the root to $*REPO

SYNOPSIS
========

    # say your code lives in /sandbox/you/project/frobnicate
    # and depends on code in ./project/lib and ./frobnicate/lib
    # and you want your ./t code to use ./t/lib modules with a
    # it of extra boilerplate and you don't feel like hardcoding
    # all of this into "use lib" statements that break with
    # every new project or require updating for every new level
    # of test or project directory.

    use FindBin::libs;

    use Some::Module;


    # make FindBin & FileSystem::Parent behave verbosely.

    use FindBin::libs :verbose;

    # make FindBin resolve the executable path before 
    # searching for lib dirs. use for this is largely 
    # in testing where a common set of lib's can be
    # used for cloned configurations.

    use FindBin::libs :resolve;

DESCRIPTION
===========

This module adds a list of existing ./lib directories above the executable path. It is a programmatic equivalent of '-I' based on where the program is running from.

My favorite example of using this is finding configuration or management modules in ./t/lib that are less likley to, say, execute destructive testing on a production database. A file running from ./project/component/t/foobar.t in joebloe's sandbox will include modules from whichever directories exist from:

    /sandbox/jowbloe/project/component/t/lib
    /sandbox/jowbloe/project/component/lib
    /sandbox/jowbloe/project/lib
    /sandbox/jowbloe/lib
    /sandbox/lib

ignoring /lib, which doesn't usually house any Perl modules.

Note that this leaves files in component/t preferring modules in t/lib over component/lib and items in component/bin ignoring the ./t/lib files entirely.

This also simplifies testing, as PERL6LIB doesn't have to be set in order to locate libs in /sandbox/jowbloe/project/component/lib.

SEE ALSO
========

  * FileSystem::Parent

Exports "scan-up" used to locate ./lib dirs starting from Bin, avoiding /lib.

  * FindBin

Exports "Bin" which locates the directory we are running from.

AUTHOR
======

Steven Lembark <lembark@wrkhors.com>

COPYRIGHT AND LICENSE
=====================

Copyright 2018-2019 Steven Lembark

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

