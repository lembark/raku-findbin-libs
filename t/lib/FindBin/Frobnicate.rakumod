use v6.d;

unit module FindBin::Frobnicate:ver<0.0.1>:auth<CPAN:lembark>;

constant FrobPath is export( :DEFAULT )
= 't/lib/FindBin/Frobnicate.pm6';
