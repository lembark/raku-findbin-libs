use v6.d;

unit module FindBin::Frobnicate:ver<0.3.0>:auth<CPAN:lembark>;

constant FrobPath is export( :DEFAULT )
= 'lib/FindBin/Frobnicate.pm6';

